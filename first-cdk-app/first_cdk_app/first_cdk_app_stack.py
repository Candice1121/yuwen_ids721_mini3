from aws_cdk import (
    # Duration,
    Stack,
    # aws_sqs as sqs,
)
from constructs import Construct
import aws_cdk.aws_s3 as s3
import aws_cdk as cdk

class FirstCdkAppStack(Stack):
    # create a function that create S3 bucket with versioning
    def create_s3_bucket(self):
        return s3.Bucket(self, "MyFirstBucket",versioned=True)

    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        # call the function to create S3 bucket
        bucket = self.create_s3_bucket()