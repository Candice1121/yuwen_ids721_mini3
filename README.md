# Yuwen_IDS721_mini3



## Getting started
This project creates an S3 Bucket using CDK with AWS CodeWhisperer. Following are the detailed requirements:
1. Create S3 bucket using AWS CDK
2. Use CodeWhisperer to generate CDK code
3. Add bucket properties like versioning and encryption

This project follows the tutorial by AWS Documentation: [Your first AWS CDK app](https://docs.aws.amazon.com/cdk/v2/guide/hello_world.html)

## Steps

### Set Up Local Environment
1. install python CDK package

    ``` python -m pip install aws-cdk-lib```

2. install the AWS CDK

    ```npm install -g aws-cdk```

### Create the app
1. make directory and navigate to that dir

    ```mkdir first-cdk-app```

    ```cd first-cdk-app```

2. initialize the app and activate Python virtual environment

    ```cdk init app --language python```

    ```source .venv/bin/activate```

    ```python -m pip install -r requirements.txt```

3. list stacks in the app
It should show the app name, for my example: FirsrtCdkAppStack

![stack](imgs/stack.png)

### Add an Amazon S3 bucket using Codewhisper
Make sure to sign in with codewhisper before using it. 

User guide for using it with VSCode: [Getting started with CodeWhisperer in VS Code and JetBrains](https://docs.aws.amazon.com/codewhisperer/latest/userguide/whisper-setup-ide-devs.html)

1. In ```first-cdk-app/first_cdp_app/first_cdk_app_stack.py```

add following imports:

    ``` 
    import aws_cdk as cdk
    import aws_cdk.aws_s3 as s3
    ```

2. use codewhisper to generate a new function

    ``` # create a function to create a new s3 bucket with versioning```

This prompt will give you a complete function that creates a S3 bucket with name "MyFirstBucket" and call the bucket in init function.
![codewhisper](imgs/codewhisper.png)

### Deploy
1. synthesize an AWS CloudFormation template

    ```cdk synth```

2. Bootstrapping before deployment: deploy stacks with AWS CDK requires dedicated Amazon S3 bucks and other containers to be avaialble to AWS CloudFormation during deployment

    ```cdk bootstrap```

3. deploy the stack

    ```cdk deploy```

## Result
### S3 bucket created
![S3](imgs/s3.png)

### S3 Versioning Enabled
![S3_version](imgs/s3_versioning.png)

### S3 Default Encryption
![S3_encrypt](imgs/s3_encryption.png)
